# This is the file you'll use to submit most of Lab 0.

# Certain problems may ask you to modify other files to accomplish a certain
# task. There are also various other files that make the problem set work, and
# generally you will _not_ be expected to modify or even understand this code.
# Don't get bogged down with unnecessary work.


# Section 1: Problem set logistics ___________________________________________

# This is a multiple choice question. You answer by replacing
# the symbol 'fill-me-in' with a number, corresponding to your answer.

# You get to check multiple choice answers using the tester before you
# submit them! So there's no reason to worry about getting them wrong.
# Often, multiple-choice questions will be intended to make sure you have the
# right ideas going into the problem set. Run the tester right after you
# answer them, so that you can make sure you have the right answers.

# What version of Python do we *recommend* (not "require") for this course?
#   1. Python v2.3
#   2. Python v2.5 or Python v2.6
#   3. Python v3.0
# Fill in your answer in the next line of code ("1", "2", or "3"):

ANSWER_1 = 'fill-me-in'


# Section 2: Programming warmup _____________________________________________

# Problem 2.1: Warm-Up Stretch

def cube(x):
    return x ** 3

def factorial(x):
    if x < 0:
        raise Exception("Factorial requires a non-negative argument.")
    elif x == 0:
        return 1
    else:
        return x * factorial(x - 1)

def count_pattern(pattern, lst):
    #Use sliding windows to examine whether a sublist is identical
    #to the given pattern.
    window_size = len(pattern)
    count = 0
    for i in range (0, len(lst)):
        #Slice out a window-sized portion of the list.
        sublist = lst[i : i+window_size]
        if pattern == sublist:
            count += 1
    return count

# Problem 2.2: Expression depth

def depth(expr):
    #Avoid counting an empty list as an expression.
    if expr and isinstance(expr, (list, tuple)):
        #Compute the depth of the head of the list then the tail of the list
        # (This applies depth to every member of the list -- manual mapping)
        head = depth(expr[0])
        tail = depth(expr[1:len(expr)])
        
        # The depth of a tuple/list of atoms like (+, 2, 3) is 1, but the depth
        # of its members is 0. To make sure we return the depth for the smallest
        # expressions possible, we take the max of the head + tail depth,
        # or 1.
        # This will exhibit some strange behavior if the list is malformed, for
        # instance the list (9, 10) will return a depth of 1 even though there's
        # no operator.
        return max(head + tail, 1)
    
    # Otherwise we're trying to examine something that isn't a list -- i.e. an
    # atom -- and therefore it can't be an expression, so it has 0 depth.
    else:
        return 0
    
    #A simpler version with map:
    #if not isinstance(expr, (list, tuple)):
    #    return 0
    #else:
    #   return max(map(depth, expr)) + 1

# Problem 2.3: Tree indexing

def tree_ref(tree, index):
    if not index:
        return tree
    return(tree_ref(tree[index[0]], index[1:]))


# Section 3: Symbolic algebra

# Your solution to this problem doesn't go in this file.
# Instead, you need to modify 'algebra.py' to complete the distributer.

from algebra import Sum, Product, simplify_if_possible
from algebra_utils import distribution, encode_sumprod, decode_sumprod

expr = Product([Product(['x', 'y', 3]), Sum(['x', 3])])
print(expr.simplify())