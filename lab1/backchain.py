from production import AND, OR, NOT, PASS, FAIL, IF, THEN, \
     match, populate, simplify, variables, instantiate
from zookeeper import ZOOKEEPER_RULES

# This function, which you need to write, takes in a hypothesis
# that can be determined using a set of rules, and outputs a goal
# tree of which statements it would need to test to prove that
# hypothesis. Refer to the problem set (section 2) for more
# detailed specifications and examples.

# Note that this function is supposed to be a general
# backchainer.  You should not hard-code anything that is
# specific to a particular rule set.  The backchainer will be
# tested on things other than ZOOKEEPER_RULES.


def backchain_to_goal_tree(rules, hypothesis):
    #Hypothesis is the goal. We want to find all the rules that are necessary
    #to reach that.

    #Something to hold every possible rule that can fit.
    NODES = [hypothesis]
    #vars = {}
    cont = False

    #Iterate through the rules and look for the ones with the hypothesis
    for rule in rules:
        for consequence in rule.consequent():
            r = match(consequence, hypothesis)
            if (r != None):
                #vars.update(r)                
                #Add to the big OR node all rule antecedents that result in this conclusion
                ant = rule.antecedent()
                l = []
                if isinstance(ant, AND):
                    for x in ant:
                        l.append(backchain_to_goal_tree(rules, instantiate(x, r)))
                    NODES.append(AND(l))
                elif isinstance(ant, OR):
                    for x in ant:
                        l.append(backchain_to_goal_tree(rules, instantiate(x, r)))
                    NODES.append(OR(l))
                else:
                    NODES.append(backchain_to_goal_tree(rules, instantiate(ant, r)))              
                cont = True
    #We made at least one addition to the NODES, so recurse for each clause in each antecedent.
    #if cont:
    #    for antecedent in NODES:
    #        for clause in antecedent:
    #            return OR(NODES + AND(backchain_to_goal_tree(rules, clause)))

    return simplify(OR(NODES))

# Here's an example of running the backward chainer - uncomment
# it to see it work:
#print backchain_to_goal_tree(ZOOKEEPER_RULES, 'opus is a zebra')
