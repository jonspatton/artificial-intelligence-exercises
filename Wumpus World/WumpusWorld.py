#################################################################################
# Jon S. Patton
# 2019 Creative Commons
# See the readme document for a high-level overview and analysis.
#################################################################################
#################################################################################
# Contents:
# 1. WumpusWorld
# 2. main
#################################################################################

from WumpusBoard import WumpusBoard
from Explorer import Explorer
from wumpus_utils import *

class WumpusWorld(object):
    '''The "game" runner. Creates a Board and an Explorer to explore it.'''
    # Attributes
    # -------------------------------------------------------------------------------------------
    explorer = None # Our intrepid hero.
    board = None    # The world to explore
    # -------------------------------------------------------------------------------------------
    def __init__(self, size = 10, starting_position = (0, 0)):
        '''
        Constructor
        Accepts optional size and starting_position.
        Default size is 10
        Default starting_position is (0,0)
        '''
        self.board = WumpusBoard(size, starting_position)
        self.explorer_position = self.board.starting_position
        self.explorer = Explorer(self.board, self.explorer_position)
        self.explorer.explore()

    def is_win():
        '''Checks if the explorer is in the square with the Gold.'''
        return self.explorer.position == self.board.gold_location
    
    def is_alive():
        '''Checks if the Explorer is still exploring.'''
        return self.explorer.alive

    ################################################
    # Representations.
    ################################################
    def __str__(self):
        return self.board.draw()

    def __repr__(self):
        return self.__str__()

    def draw(self):
        str(self)

import argparse
def main():
    '''
    Takes optional command line arguments -s (for size) and -sp (for starting_position),
    then creates a WumpusWorld.
    '''
    parser = argparse.ArgumentParser()

    parser.add_argument("--size", "-s", type = int, dest = "size", required = False, default = 10,
                        help = "An integer, size is the length of the board.")
    parser.add_argument("--starting_position", "-sp", nargs = 2, type = int, dest = "starting_position", 
                        required = False, default = [0, 0],
                        help = "Two integers, an x coordinate and y coordinate, entered as e.g.: WumpusWorld -sp 0 0.\n\
                        Must be >=3")

    args = parser.parse_args()
    if args.size <= 2:
        error: print("Board size must be >= 3.")
        return
    starting_position = tuple(args.starting_position)
    world = WumpusWorld(args.size, starting_position)

main()