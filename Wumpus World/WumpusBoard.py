#################################################################################
# Jon S. Patton
# 2019 Creative Commons
# See the readme document for a high-level overview and analysis.
#################################################################################

#################################################################################
# Contents:
# WumpusBoard
#################################################################################

import random
import time
from Explorer import Explorer
from wumpus_utils import *
from Dangers import *
from Rewards import *

'''
Wumpu World!

The rules are simple:
1. The world consists of pits, safe squares, a single Wumpus, and a single pile of gold.
2. Our explorer wants to find the gold without dying. As soon as they find the gold, they win.
3. Our explorer dies if they enter a square with the Wumpus or a pit.
4. Pits give off a breeze to adjascent squares, while Wumpuses give off a stench.
5. Our explorer has a single arrow, which they can shoot. The arrow travels in a straight line 
until it hits a wall or the Wumpus. If the arrow hits the Wumpus, the Wumpus dies.
'''

class WumpusBoard(object):
    '''
    The representation of the world for the Explorer to explore.
    '''
    # Attributes
    # -------------------------------------------------------------------------------------------
    board = [[]]            # The world to explore
    wumpus = None
    gold = None
    size = 0
    #special = []           # Special characters, e.g. !, ~, W, etc.
    pits = []               # Locations of all the pits in the world. (mostly used for debugging)
    # -------------------------------------------------------------------------------------------

    def __init__(self, size = 10, starting_position = (0, 0)):
        #################################################################################
        '''
        Constructor.
        Size - the length of the walls. The board is a square.
        Starting position - Where the explorer enters the cave. Usually a corner.
        '''
        #################################################################################
        self.size = size
        self.starting_position = starting_position

        # Build an nxn board.
        self.board = [] * size
        [self.board.append([' '] * size) for x in range(size)]

        # Generate a list of random x, y coordinates (not including start)
        coordinates = getCoordinateSet(self.size)
        coordinates.remove(starting_position)
        random.shuffle(coordinates)
    
        self.placeDangers(coordinates)

        # Gold location. Some wumpus worlds allow this to be unfair by putting 
        # the gold literally anywhere. We'll try to be a little nicer.
        self.gold = Gold(coordinates.pop())
        self.__addToSquare__(self.gold_rep(), self.gold.location)

        #self.special = [self.pit_rep(), self.wumpus_rep(), self.stink_rep(),
        #                self.breeze_rep(), self.gold_rep()]


    def placeDangers(self, coordinates):
        #################################################################################
        ''' Populate the world with dangers like the Wumpus and Pits.'''
        #################################################################################
        # We want at least a couple pits or it's too easy, but we don't want too many.
        # Since the grid is nxn, it seems fine to have n pits.
        self.placePits(coordinates, random.randint(self.size//2, self.size - 2))

        #Wumpus can be anywhere except the starting position.
        self.placeWumpus(coordinates)

    def placePits(self, coordinates, num_pits):
        for i in range(1, num_pits):
            pit = Pit(coordinates.pop())
            #self.pits.append(pit)
            self.__surround__(pit.perceptionIndicator(), pit.location)
            self.__addToSquare__(str(pit), pit.location)

    def placeWumpus(self, coordinates):
        wumpus_location = coordinates.pop()
        self.wumpus = Wumpus(wumpus_location)
        self.__addToSquare__(self.wumpus_rep(), wumpus_location)
        #add stink lines
        self.__surround__(self.stink_rep(), wumpus_location)

    def __surround__(self, c, loc):
        #################################################################################
        ''' Surrounds a location loc with a character c'''
        #################################################################################
        x = loc[0]
        y = loc[1]
        surrounding_squares = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
        for square in surrounding_squares:
            a = square[0]
            b = square[1]
            if 0 <= a < self.size and 0 <= b < self.size:
                self.__addToSquare__(c, (a, b))

    def __addToSquare__(self, c, loc):
        #################################################################################
        ''' Adds a character c to a location loc.'''
        #################################################################################
        x = loc[0]
        y = loc[1]
        if 0 <= x < self.size and 0 <= y < self.size:
            self.board[x][y] = list(c + "".join(self.board[x][y]).strip(' '))
    
    def addToSquare(self, c, loc):
        #################################################################################
        '''
        Adds a character c to a location loc.
        Allows us to do some checks that the update is safe before calling the private 
        version.
        '''
        #################################################################################
        self.__addToSquare__(c, loc)

    def __removeFromSquare__(self, c, loc):
        #################################################################################
        ''' Removes the first instance of a character c from a location loc.'''
        #################################################################################
        x = loc[0]
        y = loc[1]
        if 0 <= x < self.size and 0<= y < self.size:
            if c in self.board[x][y]:
                self.board[x][y].remove(c)

            #If it's empty, make it a space.
            if not self.board[x][y]:
                self.board[x][y] = (' ',)
    
    def removeFromSquare(self, c, loc):
        #################################################################################
        ''' Removes the first instance of a character c from a location loc.'''
        #################################################################################
    
        self.__removeFromSquare__(c, loc)

    def containsWumpus(self, loc):
        #################################################################################
        ''' Returns whether loc the Wumpus's position'''
        #################################################################################
    
        x = loc[0]
        y = loc[1]
        if 0 <= x < self.size and 0 <= y < self.size:
            return  self.wumpus_rep() in self.board[loc[0]][loc[1]]
        else:
            return False
    
    def killWumpus(self):
        #################################################################################
        ''' Remove the Wumpus character from the board and "announce" that he's dead'''
        #################################################################################
        loc = self.wumpus.getLocation()
        x = loc[0]
        y = loc[1]
        self.removeFromSquare(self.wumpus_rep(), loc)
        self.wumpus.killWumpus()

        # A dead wumpus might still stink, but we want the explorer to ignore that.
        surrounding_squares = getSurroundingSquares(loc)
        for square in surrounding_squares:
            if 0 <= x < self.size and 0 <= y < self.size:
                self.removeFromSquare(self.stink_rep(), square)
    
    def isWumpusAlive(self):
        #################################################################################
        ''' Reports whether the Wumpus is still alive.'''
        #################################################################################
    
        return self.wumpus.isAlive

    def getSquareInfo(self, loc):
        #################################################################################
        ''' Returns the information on a square.'''
        #################################################################################
        return self.board[loc[0]][loc[1]]

    def dangerCost(self, s, danger = Danger()):
        #################################################################################
        '''
        Returns the cost function of a Danger
        danger - a danger object (which can provide its costs)
        s - a string 'horrible', 'cost', or 'per' for the type of cost
        '''
        #################################################################################
        return danger.cost(s)

    #################################################################################
    # Representations for special types of things in the world and "constants".        
    #################################################################################
    def gold_rep(self):
        return str(self.gold)

    def pit_rep(self):
        return str(Pit())
     
    def wumpus_rep(self):
        return str(self.wumpus)

    def breeze_rep(self):
        return Pit().perceptionIndicator()
    
    def stink_rep(self):
        return self.wumpus.perceptionIndicator()

    def square_rep(self, i, j):
        return self.board[i][j]

    #################################################################################
    # A formatted Wumpus Board to print.
    #################################################################################        
    def __str__(self):
        s = "|"
        for i in range(0, self.size):
            s = s + '---'
        s += '|\n'
        for row in range(0, self.size):
            s += '|'
            for col in range(0, self.size):
                s += " " + self.square_rep(row, col)[0] + " "
            s += '|\n'
        s += '|'
        for i in range(0, self.size):
            s += '---'
        return s + '|'

    def __repr__(self):
        return str(self)
    
    def draw(self):
        print(self)
