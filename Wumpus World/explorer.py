#################################################################################
# Jon S. Patton
# 2019 Creative Commons
# See the readme document for a high-level overview and analysis.
#################################################################################

#################################################################################
# Contents:
# 1. Explorer
# 2. JournalEntry (not used)
#################################################################################

import random
import time
from wumpus_utils import *
from Arrow import *

class Explorer(object):
    '''
    Our intrepid hero.
    Searches a given WumpusWorld until he finds the gold or dies.
    '''
    # Attributes
    # -------------------------------------------------------------------------------------------
    # Whether we continue doing the thing.
    exploring = True

    # Just in case we want to know this for some reason.
    alive = True

    # What we're exploring.
    board = None

    # The explorer's current location.
    position = None

    arrow = None
    
    # All the coordinates in a list
    coordinates = []

    # For printing out a nice message about how we're moving!
    dir_words = {(1, 0) : 'south', (0, 1) : 'east', (-1, 0) : 'north', (0, -1) : 'west'}

    # To get what the explorer knows about a position, ask for
    # journal[square], where square is a (x,y) coordinate.
    # The journal can contain dictionaries for each square
    # with the following:
    # 1. Special characters like W, o, !, ~
    # 2. True or False values for known qualities. E.g. If we know
    # the wumpus is in the square, the dictionary will contain W:True.
    # 3. pcount, a special entry if something has indicated that a square has a pit.
    #    If four different squares have indicated that a square has a pit, it certainly
    #    has a pit.
    # 4. wcount, a special entry if something has indicated that a square has the Wumpus.
    #    If two different squares indicate that something has the Wumpus, it definitely
    #    has the Wumpus.
    # 
    # If the square might contain something dangerous, it's marked with '?'
    # Be careful that python treats '?' as "True-y" for evaluations.
    # So if you ask "if W" and have W:'?', it'll read as true.
    # Also, be sure to check whether the square's information actually contains
    # an entry for, say, W before trying to use it, since "None" is False-y.
    journal = {}

    # Our explorer needs a few things:
        #1. A way to move.
        #2. A way to decide how to move.
        #3. Knowledge about the world so they can decide how to move.
        #4. An arrow to fire.
    # -------------------------------------------------------------------------------------------

    def __init__(self, board, starting_position = (0, 0)):
        ####################################################################
        ''' 
        Constructor. Takes a WumpusBoard and an optional starting square
        on that board.
        '''
        ####################################################################
        self.board = board
        self.position = starting_position
        self.arrow = Arrow(self.board)
        
        self.coordinates = [(x, y) for x in range(0, self.board.size) for y in range(0, board.size)]

        for coordinate in self.coordinates:
            self.journal.update({coordinate : {self.vis() : False}})
        
        # Our intreptid explorer.
        self.board.addToSquare(self.__repr__(), starting_position)
        self.visitSquare(starting_position)
        self.board.draw()
        time.sleep(0.5)

    def explore(self):
        ####################################################################
        ''' Do the thing until you don't do the thing anymore.'''
        ####################################################################
        while(self.exploring):
            self.decide()
    
    ####################################################################
    # Visiting a square involves updating what we know about that square,
    # deciding if we die or win, then evaluating the nearby squares, and
    # finally making a decision about how to move.
    ####################################################################
    def visitSquare(self, position):
        #Return early if we've already been here.
        k = self.journal[position]
        if k and k[self.vis()]:
            k[self.vis()] += 1
            return

        squareInfo = self.board.getSquareInfo(position)
        b = self.board.breeze_rep()
        s = self.board.stink_rep()
        w = self.board.wumpus_rep()
        p = self.board.pit_rep()
        g = self.board.gold_rep()
        
        #We ded.
        if w in squareInfo:
            self.killExplorer()
            print("Oh no! The explorer ran into the Wumpus and got eated!")
            return
        
        if p in squareInfo:
            self.killExplorer()
            print("Oh no! The explorer fell into a pit and went splat!")
            return

        #We win.
        elif g in squareInfo:
            self.pickUpGold()
            print("The explorer wins!")
            return

        # We survived but didn't win. Time to write in our journal what we've learned!
        self.journal[position].update({self.vis(): 1, w: False, p: False})

        if s in squareInfo:
            self.journal[position].update({s:True})
            # Mark the adjacent unvisited squares for possible Wumpus
            self.markPossibleDangers(position, w, s, self.wcount_rep())
        else:
            self.noDangersAround(w, position)

        if b in squareInfo:
            breezes = squareInfo.count(b)
            self.journal[position].update({b:True, 'breezeCount' : breezes})
            # Mark the adjacent unvisited squares for possible pits
            self.markPossibleDangers(position, p, b, self.pcount_rep())
        else:
            self.noDangersAround(p, position)

    def markPossibleDangers(self, loc, danger_symbol, percept_symbol, countKey):
        ###############################################################
        '''
        The methods this calls will:
        1. Mark surrounding squares with possible pits; or
        2. Ascertain that there is certainly a pit in an adjacent square.
           This is done by noticing that a pit is surrounded by at most
           Four squares with breezes. If our explorer ever accounts for
           all squares surrounding a possible pit, then that pit becomes
           definite and they know to never go there if they can help it.
        
        Inputs: 
        loc -- the location where the danger was sensed (usually explorer's 
            position)
        danger_symbol -- the symbol for e.g. a pit or Wumpus
        percept_symbol -- the symbol for e.g. a breeze or stink
        countKey -- e.g. wcount or pcount (keeps track of how often this 
                    square was possibly dangerous)
        '''
        ####################################################################
        #p = self.board.pit_rep()
        size = self.board.size
        surrounding_squares = getSurroundingSquares(loc)
        for square in surrounding_squares:
            if square in self.journal:
                self.analyzeDangerCount(square, danger_symbol, countKey)
                self.analyzeSurroundingSquares(percept_symbol, square)

    def analyzeDangerCount(self, square, symbol, countKey):
        ###############################################################
        '''
        We're near a danger and we need to update our journal entries
        for every square we can figure out something for.
        
        square -- the square we will be examining.
        symbol -- the danger symbol for that square. (We do pits and
        the Wumpus separately).
        countKey -- the dictionary key for the counting variable,
        e.g. pcount or wcount. (Maybe there's a better way to do that.)
        '''
        ###############################################################
        knowledge = self.journal[square]
        p = self.board.pit_rep()
        w = self.board.wumpus_rep()

        # If we've been here before, we've already done this stuff.
        if knowledge[self.vis()]:
            return
        
        #Otherwise we'll start off by marking the hazard as possible
        if symbol not in knowledge:
            self.journal[square].update({symbol : '?'})
        
        # Some other square already marked this with '?' ...
        if countKey in knowledge:
            count = knowledge[countKey] + 1
            self.journal[square].update({countKey : count})

            # The square could contain a pit or Wumpus but almost
            # certainly contains a pit. See notes on morePthanW.
            if self.morePthanW(square):
                self.journal[square].update({self.board.wumpus_rep(): False})
                self.journal[square].update({self.board.pit_rep(): True})
                return

            # threshold for determining the excact nature of a danger.
            # A pit in a corner can be identified by two squares.
            # A pit on an edge can be identified by three squares.
            if symbol == self.board.wumpus_rep() or isInCorner(square, self.board.size):
                threshold = 2
            elif isOnEdge(square, self.board.size):
                threshold = 3
            else:
                threshold = 4

            if count >= threshold:
                self.journal[square].update({symbol: True, 'threshold' : threshold})

                # The Wumpus is not in a pit according to the construction of the board.
                # (We still check that we didn't already identify the Wumpus's square --
                # the explorer will have to shoot it AFTER figuring out it's there
                # and there could still be a pit next to him as well.)
                if symbol == p:
                    if self.journal[square][w] != True:
                        self.journal[square].update({w: False})
                    #If I just IDed a pit and there's only one breeze, there can't be another pit.
                    knowledge = self.journal[self.position]
                    breezeCount = knowledge['breezeCount']
                    surrounding_squares = getSurroundingSquares(self.position)
                    for s in surrounding_squares:
                        if s in self.journal and s != square and breezeCount <= 1:
                            self.journal[s].update({p : False})
                elif symbol == w:
                    self.journal[square].update({p: False})
                    self.wumpusIn(square)

        # This is the first time the square was examined for this danger,
        # so set the count to 1.
        else:
            self.journal[square].update({countKey : 1})
    
    def morePthanW(self, square):
        ###############################################################
        '''
        If a square could have the Wumpus OR a pit,
        then we can conclude that it's a pit if pcount >= 2
        while wcount is only 1.
        |----------|
        | W  (!)   |
        | !~> X    |
        | 1,2 ~^   |
        |----------|
        '''
        ###############################################################
        knowledge = self.journal[square]
        if self.pcount_rep() in knowledge and self.wcount_rep() in knowledge:
            pcount = knowledge[self.pcount_rep()]
            wcount = knowledge[self.wcount_rep()]
            if pcount >= 2 and wcount == 1:
                return True
        return False
    

    def analyzeSurroundingSquares(self, percept_symbol, square):
        ########################################################################
        '''
        We can also glean some information by examining the DIAGONAL squares.
        Say the explorer is in this position (. is the square they came from,
        1-4 are diagonal squares):
        |--------|
        | 3 ? 1  |
        | ? X ?  |
        | 4 . 2  |
        |--------|
        The explorer can look in their journal for each of those 4 squares
        Examine the journal for square 2.
        If there's no breeze there, then the ? to the right can't be a pit.
        If there IS a breeze there, then we don't change anything (we already
        noted in the journal that there's 2 squares that say there's a pit there)
        
        percept_symbol -- the symbol indicating a danger, e.g. ! for Wumpus
        square -- the square whose neighbors are being examined and marked.
        '''
        ######################################################################
        surrounding_squares = getSurroundingSquares(square)
        size = self.board.size
        for s in surrounding_squares:
            if s in self.journal:
                knowledge = self.journal[s]
                if knowledge[self.vis()]:
                    if percept_symbol not in knowledge:
                        self.journal[square].update({percept_symbol : False})

    def noDangersAround(self, symbol, square):
        ####################################################################
        '''
        Marks all adjacent squares as safe from a particular danger.
        symbol -- the journal entry key for the danger, e.g. p for pit.
        square -- the square whose neighbors are getting marked.
        '''
        ####################################################################
        size = self.board.size
        surrounding_squares = getSurroundingSquares(square)
        for s in surrounding_squares:
            if s in self.journal:
                # Don't need to check if the square is visited, because a visited
                # square definitely won't have a pit (i.e. already marked false).
                self.journal[s].update({symbol : False})         

    def wumpusIn(self, square):
        ###############################################################
        '''
        The wumpus has been found; set every other square to no Wumpus
        "square" is the square containing the Wumpus
        '''
        ###############################################################
        w = self.board.wumpus_rep()
        self.journal[square].update({w : True})
        for coordinate in self.coordinates:
            if coordinate != square:
                self.journal[coordinate].update({w : False})

    def decide(self):
        ########################################################################################
        '''
        Decision algorithm, cost function, and random movement criteria.
        Returns a move() or, in very special circumstances, another decide().
        
        To decide where to go, the explorer uses the following:
        0. V = {(1, 0), (0, 1), (-1, 0), (0, -1)} is the available direction vectors.
        1. The explorer will think about moving in the direction v = V[i = 0] first.
        2. They will check what they know about square position+v.
        3. If it might contain a pit, they assign a penalty to moving there.
        4. If it might contain a Wumpus, they assign a penalty to moving there.
        5. If it DEFINITELY contains a Wumpus, they assign a bonus to firing their arrow.
        6. If it DEFNITELY contains gold, they will assign the largest possible bonus
           to moving there.
        7. If they've already visited the square, it gets a moderate penalty.
        8. Otherwise, until we have exhausted V, GOTO 2 with v = V[i+1].
        9. Move to the square with the smallest penalty. Break ties by taking the earliest
           (this means that the explorer would generally try to move clockwise first).
        '''
        ########################################################################################

        if not self.exploring:
            print("Done.")
            return

        # Movement vectors and a dictionary to keep track of 
        V = [(1, 0), (0, 1), (-1, 0), (0, -1)]
        moves = {}
        
        # Convenient aliases
        w = self.board.wumpus_rep()
        p = self.board.pit_rep()

        # If we're next to a Wumpus, there are situations where we need to fire our
        # arrow and hope for the best.
        possibleArrowDirections = []

        # Costs
        # -----------------------------------------------------------------------------------------
        unvisCost = -3      # An unvisited square is likely to be slightly better than some others.
        safeCost = -2       # A safe square is good, especially if we haven't visited it yet.

        visCost = 1         # Ensures that the cost is positive if the square has been visited.
        perVisit = 50       # Penalize visiting the same squares over and over.

        perWump = self.board.dangerCost('per', self.board.wumpus) # Helps decide if we should shoot yet
        perPit = self.board.dangerCost('per')           # Work out if a pit is more likely in a square.
        dangerCost = self.board.dangerCost('cost')      # When we *could* run into a pit or a Wumpus 
        horrible = self.board.dangerCost('horrible')    # When we will almost definitely hit a pit or a wumpus 
        
        # Big enough that it won't accidentally choose a pit instead.
        randThresh = dangerCost - perPit
        # -----------------------------------------------------------------------------------------

        for v in V:
            cost = 0
            square = tuple(map(lambda x, y: x + y, v, self.position))
            if square in self.journal:
                #What do we know about the square position + v?
                knowledge = self.journal[square]
                #Prioritize unvisted safe squares.
                if self.isSafe(square):
                    cost += safeCost
                    if knowledge[self.vis()]:
                        cost += visCost + perVisit * knowledge[self.vis()]
                elif not knowledge[self.vis()]:
                    cost += unvisCost

                # If it's *definitely* the Wumpus, we can fire the arrow now.
                if w in knowledge:
                    if knowledge[w] == True:
                        print("A Wumpus is ahead!")
                        # If we hit it, it's fine.
                        if self.fireArrow(v, self.position):
                            cost += 0
                        # Otherwise severely penalize moving there,
                        # because something is seriously wrong ...
                        # (or we've already fired our arrow and missed)
                        else:
                            print("Eek!")
                            cost += horrible
                    elif knowledge[w] == '?':
                        # Don't want to walk there, but we might 
                        # decide to fire our arrow there later.
                        cost += dangerCost + perWump * knowledge[self.wcount_rep()]
                        possibleArrowDirections.append(v)
            
                if p in knowledge:
                    if knowledge[p] == True:
                        cost += horrible
                    elif knowledge[p] == '?':
                        cost += dangerCost + perPit * knowledge[self.pcount_rep()]

                moves[v] = moves.get(v, 0) + cost

        # Find the minimum entries in our possible moves and the minimum value.        
        min_cost_directions = mins(moves)
        minimum_cost = moves[min_cost_directions[0]]

        l = len(possibleArrowDirections)
        #If we have a single lowest-cost move, it's clearly the best.
        if l == 1:
            direction = min_cost_directions.pop()
            print("I think it's safe to go", self.dir_words[direction])
            return self.move(direction)
        
        # If we were next to a Wumpus and it's preventing us from moving, then
        # we can at least shoot an arrow and hope it kills the Wumpus. If it does,
        # then we can safely move in that direction.
        # So we prune the possible moves by finding out which might contain the
        # Wumpus, and of those, which are most likely to contain the Wumpus.
        elif l > 0 and minimum_cost > randThresh and self.arrow:
            if l == 1:
                self.fireArrow(possibleArrowDirections[0], self.position)
            elif l >= 2:
                print("I hope this works!")
                for direction in possibleArrowDirections:
                    if direction not in min_cost_directions:
                        possibleArrowDirections.remove(direction)
                if possibleArrowDirections:
                    random.shuffle(possibleArrowDirections)
                    direction = possibleArrowDirections.pop(0)
                    shot = self.fireArrow(direction, self.position)
                    # Corner case: We missed the Wumpus, but there were two or more
                    # squares he *could* have been in. (This is certain to mark
                    # the Wumpus correctly if there were only two directions,
                    # such as when the Wumpus is next to the Explorer's starting corner.)
                    if not shot and len(possibleArrowDirections) >= 1:
                        for direction in possibleArrowDirections:
                            square = tuple(map(lambda x, y: x + y, direction, self.position))
                            #print(square, self.board.wumpus.location)
                            print("The Wumpus is to the ", self.dir_words[direction])
                            self.wumpusIn(square)
            #Recurse before actually moving, now that we have some additional information.
            return self.decide()
        
        # Final fall-through if not safe is to move randomly and hope for the best.
        # To be fair, they will say this at some incorrect times, like when they
        # have visited neighboring safe squares so many times that the cost function
        # is greater than that for moving in a potentially dangerous direction.
        random.shuffle(min_cost_directions)
        direction = min_cost_directions.pop()
        if minimum_cost > randThresh:
            print("I can't figure out which way is safe! Moving", \
            self.dir_words[direction], "and hoping for the best ...")
        
        # The explorer determined multiple safe moves but has
        # no preference
        else:
            if max(moves.values()) > 450:
                print("A little dangerous around here! But I think it's safe to go", self.dir_words[direction])
            else:
                print("Seems safe all around! Moving", self.dir_words[direction])
        return self.move(direction)

    def isSafe(self, square):
        ###############################################################
        '''
        Read the journal entry for a square to check if it definitely
        doesn't have a Wumpus or Pit.
    
        Returns True iff the square has beed absolutely determined to
        be free of danger.'''
        ###############################################################
        knowledge = self.journal[square]
        p = self.board.pit_rep()
        w = self.board.wumpus_rep()
        if (p in knowledge and w in knowledge and not (knowledge[p] or knowledge[w])) \
            or (p in knowledge and not knowledge[p]) \
            or w in knowledge and not knowledge[w]:
            return True
        return False
    
    def move(self, direction):
        ###############################################################
        '''
        Take a step in a given direction, then visit the square.
        Direction is a vector like (1, 0), which tells the explorer to move down.
        '''
        ###############################################################
        self.board.removeFromSquare(self.__str__(), self.position)
        self.position = tuple(map(lambda x, y: x + y, direction, self.position))
        self.board.addToSquare(self.__str__(), self.position)
        self.board.draw()
        time.sleep(0.5)
        self.visitSquare(self.position)
    
    def fireArrow(self, direction, position):
        ###############################################################
        ''' 
        Fire the arrow, and glean all position information from doing
        so. If the Wumpus dies, we can clean up all our Journal entries
        to reflect that every square is safe from the Wumpus.
        
        If the Wumpus doesn't die, then the best we can do is mark
        a single row or column safe from the Wumpus.

        Returns True iff the Wumpus is killed.
        '''
        ###############################################################
        if not self.arrow:
            return False
        
        print("Firing to the", self.dir_words[direction], "!")
        time.sleep(0.5)

        if self.arrow:
            print("Fire!")
            self.arrow.fire(direction, position)
            self.arrow = None
        alive = self.board.isWumpusAlive()
        
        # If the wumpus died, then we can safely set every journal entry to w: False
        if not alive:
            for coordinate in self.coordinates:
                w = self.board.wumpus_rep()
                self.journal[coordinate].update({w : False})
            wumpusLocation = tuple(map(lambda x, y: x + y, direction, position))
            # The wumpus is not in a pit.
            # n.b. This is safe only because our Explorer never fires unless they
            # are next to the Wumpus.
            p = self.board.pit_rep()
            self.journal[wumpusLocation].update({p: False})
            return True
        # Otherwise the Wumpus lived. We know that none of the squares in the direction
        # we fired contain the Wumpus, further,  if there was only one other surrounding
        # square that *could* contain the Wumpus, that square must contain it.
        else:
            print("Oh no! I missed! The Wumpus lives!")
            pause()
            loc = position
            size = self.board.size
            w = self.board.wumpus_rep()
            while 0 <= loc[0] < size and 0 <= loc[1] < size:
                self.journal[loc].update({w : False})
                loc = tuple(map(lambda x, y: x + y, direction, loc))
        self.arrow = None
        return False

    ###############################################################
    # End States.
    ###############################################################
    def killExplorer(self):
        ''' The explorer lost. '''
        print("Auuuggh!")
        self.alive = False
        self.exploring = False

    def pickUpGold(self):
        ''' The explorer won. '''
        print("I found the gold! Hooray!")
        self.exploring = False

    ###############################################################
    # String representations, getters, and "constants"
    # (don't want to have to remember was is V or v for visited?)
    ###############################################################
    def __repr__(self):
        return str(self)
    
    def __str__(self):
        return 'X'
    
    def vis(self):
        return 'v'

    def possible_danger_rep(self):
        return '?'
    
    def pcount_rep(self):
        return 'pcount'
    
    def wcount_rep(self):
        return 'wcount'

    def getPosition(self):
        return self.position

class journalEntry(object):
    ###############################################################
    ''' 
    A class to hold information about a square
    Ended up not using this object; it would mostly duplicate
    a dictionary and make the code harder to read.
    '''
    ###############################################################
    square = {}
    def __ini__(self, square):
        self.square = None
        
    def update(self, entries):
        self.square.update(entries)

    def get(self):
        return square
