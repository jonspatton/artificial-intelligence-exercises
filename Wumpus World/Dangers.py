#################################################################################
# Jon S. Patton
# 2019 Creative Commons
# See the readme document for a high-level overview and analysis.
#################################################################################

#################################################################################
# Contents:
# 1. Danger
# 2. Wumpus
# 3. Pit
#################################################################################

from wumpus_utils import *

class Danger:
    ''' Base class for hazards in the world.'''
    costs = {}

    def __init__(self):
        self.costs = {'horrible' :  1000, 'cost' : 500, 'per' : 50}

    def cost(self, s):
        return self.costs[s]

    # Required. This is only caught at runtime for this method, unfortunately, not instantiation.
    def perceptionIndicator(self):
        raise NotImplementedError('perceptionIndicator() not implemented for ', type(self))

class Wumpus(Danger):

    #################################################################################
    ''' The big bad.'''
    #################################################################################
    # Attributes
    # -------------------------------------------------------------------------------------------
    location = None
    isAlive = True
    # -------------------------------------------------------------------------------------------
    #################################################################################
    # Constructor
    # location - starting square, e.g. an (x,y) coordinate
    #################################################################################
    
    def __init__(self, location):
        self.location = location
        super()
        self.costs['per'] = -1

    def killWumpus(self):
        #############################################################################
        ''' No longer alive and will have no location. '''
        #############################################################################
        self.isAlive = False
        self.location = None
        self.__scream__()
    
    def __scream__(self):
        #################################################################################
        ''' "When the Wumpus dies, it emits a scream that can be heard anywhere in the cavern"'''
        #################################################################################
    
        self.isAlive = False
        print("RAWRRR!!! AUGGHHH!!!!")
        print("The explorer killed the Wumpus!")
        pause()

    #################################################################################
    # Representations and getters.
    #################################################################################
    def perceptionIndicator(self):
        ''' This is the character that the Explorer uses to know this Danger is nearby.'''
        return '!'

    def __repr__(self):
        return str(self)
    
    def __str__(self):
        return 'W'

    def getLocation(self):
        return self.location

class Pit(Danger):
    #################################################################################
    ''' A hazard in the world.'''
    #################################################################################
    # Attributes
    # -------------------------------------------------------------------------------------------
    location = None
    # -------------------------------------------------------------------------------------------
   
    def __init__(self, location = None):
        #################################################################################
        ''' 
        Constructor
        location - starting square, e.g. an (x,y) coordinate
        '''
        #################################################################################
        super()
        self.location = location

    #################################################################################
    # Representations and getters.
    #################################################################################
    def perceptionIndicator(self):
        ''' This is the character that the Explorer uses to know this Danger is nearby.'''
        return '~'

    def __repr__(self):
        return str(self)
    
    def __str__(self):
        return 'o'
    
    def getLocation(self):
        return self.location