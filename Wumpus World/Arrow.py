import time
class Arrow(object):
    ''' The Explorer's Arrow.'''

    # Attributes
    # -------------------------------------------------------------------------------------------
    #direction = None    # Direction to move.
    board = None        # Board to move in.
    #position = None     # Current position.
    wasFired = False
    # -------------------------------------------------------------------------------------------

    def __init__(self, board):
    #################################################################################
        ''' 
        Constructor.
        An arrow moves through a board it's given.
        It travels in a single direction until it reaches a wall or a Wumpus.
        If it hits a wall, the arrow is destroyed.
        If it hits a Wumpus, it updates the state of the world to remove the
        Wumpus.
        Board -- a WumpusBoard for this to move through.
        Direction -- a vector like (1, 0), which tells the arrow to move down.
        Position -- where this starts moving from. 
        '''
    #################################################################################

        self.board = board
        #self.direction = direction
        #self.position = position
        #self.fire(direction)

    def fire(self, direction, position):
    #################################################################################
        ''' Moves in a constant direction until it is off the board, 
            then erases itself.'''
    #################################################################################

        #if wasFired:
        #    return
        size = self.board.size
        while 0 <= position[0] < size and 0 <= position[1] < size:
            self.board.removeFromSquare(self.__repr__(), position)
            position = tuple(map(lambda x, y: x + y, direction, position))
            self.board.addToSquare(self.__repr__(), position)
            self.board.draw()
            if self.board.containsWumpus(position):
                self.board.killWumpus()
                self.board.removeFromSquare(self.__repr__(), position)
                self.board.draw()
                break
            time.sleep(0.25)
        self.board.removeFromSquare(self.__repr__(), position)
        self.wasFired = True

    #################################################################################
    # Representations.
    #################################################################################
    def __str__(self):
        return 'A'
    def __repr__(self):
        return self.__str__()