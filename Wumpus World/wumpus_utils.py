
#################################################################################
# Jon S. Patton
# 2019 Creative Commons
# See the readme document for a high-level overview and analysis.
#################################################################################
'''
Utility functions, constants, etc. for the Wumpus World.
Basically just small functions that might be needed by
any object in the "game".
'''
#################################################################################
# Contents
# 1. getCoordinateSet
# 2. getSurroundingSquares
# 3. isInCorner
# 4. isOnEdge
# 5. mins
# 6. pause
#################################################################################

def getCoordinateSet(size):
    #################################################################################
    ''' Creates a set of (x, y) pairs for an size x size grid'''
    #################################################################################
    return [(x, y) for x in range(0, size) for y in range(0, size)]

def getSurroundingSquares(square):
    #################################################################################
    ''' 
    Return a set of tuples containing the surrounding squares 
    of an (x, y) coordinate pair square.
    '''
    #################################################################################
    x = square[0]
    y = square[1]
    return [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]

def isInCorner(square, size):
    #################################################################################
    ''' Determines if a an (x, y) coordinate square is in the corner of a grid with size'''
    #################################################################################

    x = square[0]
    y = square[1]
    a = (x == 0 and y == 0)
    b = (x == size - 1 and y == size - 1)
    c = (x == 0 and y == size - 1)
    d = (x == size - 1 and y == 0)
    return a or b or c or d

def isOnEdge(square, size):
    #################################################################################
    ''' Determines if a an (x, y) coordinate square is on an edge of a grid with size'''
    #################################################################################

    if isInCorner(square, size):
        return true
    x = square[0]
    y = square[1]
    a = x == 0 or y == 0
    b = x == size - 1 or y == size - 1
    return a or b

def mins(d):
    #################################################################################
    ''' Return a tuple containing the indices of minimum values in a dictionary d'''
    #################################################################################
    indices = []
    minimum = float("inf")

    for key, value in d.items():
        if value == minimum:
            minimum = value
            indices.append(key)
        elif value < minimum:
            minimum = value
            indices.clear()
            indices.append(key)
    return indices

def pause():
    #################################################################################
    ''' Request arbitrary user input before continuing.'''
    #################################################################################
    input("Press ENTER to continue.")
