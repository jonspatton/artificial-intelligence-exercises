\documentclass[12pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage{geometry}                		% See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   		% ... or a4paper or a5paper or ... 
%\geometry{landscape}                		% Activate for rotated page geometry
%\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}				% Use pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
								% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amsmath,amssymb,amsthm, tikz}

\usepackage{parskip}
\setlength{\parindent}{15pt}

\usepackage{setspace}
\onehalfspace

%for graphics with captions.
\usepackage{graphicx}
\usepackage{sidecap}
\usepackage{wrapfig}
\usetikzlibrary{arrows, automata}

\usepackage{hyperref}

\usepackage{mathrsfs}

%more math notation
\usepackage{mathabx}
\usepackage{mathtools}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

%For code formatting
\usepackage{listings}

%SetFonts
    \makeatletter
    
    
    %title that doesn't take up half the page
    \def\@maketitle{%
  \newpage
  %\null%ORIGINAL
  %\vskip 2em%ORIGINAL
  \begin{center}%
  \let \footnote \thanks
    {\Large\@title \par}%ORIGINAL: \LARGE
    \vskip 0.3em%NEW
    %\vskip 1.5em%ORIGINAL
    {\large
      \lineskip .5em%
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}\par}%
    \vskip 0.3em%NEW
    %\vskip 1em%ORIGINAL
    {\large \@date}%
  \end{center}%
  \par
  \vskip 0.5em%ORIGINAL
  %\vskip 1.5em%ORIGINAL
}
    
    %The sections are numeric and the subsections are alphabetical.
    \renewcommand\section{\@startsection{section}{1}{\z@}%
                                       {-3.5ex \@plus -1ex \@minus -.2ex}%
                                       {2.3ex \@plus.2ex}%
                                       {\bfseries\normalsize}}
                                       \renewcommand\thesection{\arabic{section}. }
                                       
     \renewcommand\subsection{\@startsection{subsection}{1}{\z@}%
                                       {-3.5ex \@plus -1ex \@minus -.2ex}%
                                       {2.3ex \@plus.2ex}%
                                       {\normalsize}}
                                       \renewcommand\thesubsection{\alph{subsection}. }
      \renewcommand\subsubsection{\@startsection{subsubsection}{1}{\z@}%
                                       {-3.5ex \@plus -1ex \@minus -.2ex}%
                                       {2.3ex \@plus.2ex}%
                                       {\itshape\normalsize}}
                                       \renewcommand\thesubsubsection{\alph{subsubsection}. }

\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}
%from https://stackoverflow.com/questions/3175105/writing-code-in-latex-document

\renewcommand{\qedsymbol}{$\blacksquare$}

%pmod no spaces
\makeatletter
\let\@@pmod\pmod
\DeclareRobustCommand{\pmod}{\@ifstar\@pmods\@@pmod}
\def\@pmods#1{\mkern4mu({\operator@font mod}\mkern 6mu#1)}
\makeatother


\usepackage{fancyhdr}
	\pagestyle{fancy}
	\rhead{{Patton - } \thepage}
	\lhead{Wumpus World Agent in Python}
	
	
	%for triangle
	\usepackage{tikz}
	\usetikzlibrary{positioning, calc}
\usepackage[siunitx]{circuitikz}

%graphics
\usepackage{graphicx}

\title{A Simple but Successful Rules-Based Wumpus World Agent in Python}
\author{Jon Patton}
\date{}							% Activate to display a given date or no date

\begin{document}
\maketitle
\thispagestyle{fancy}

This program is my implementation of a classic artificial intelligence (AI) problem wherein a programmed Agent, called an Explorer, solving a ``Wumpus World" grid environment. The Explorer navigates Dangers, including Pits and a monster called the Wumpus, to reach a goal, which is a pile of Gold. The Explorer possesses a single Arrow, which the Explorer can use to kill the Wumpus. The problem statement is used as context for discussing AI methods in Russell and Norvig\cite{RussellAndNorvig} and was inspired by a computer game, \textit{Hunt the Wumpus}, written by Gregory Yob in 1973\cite{wikipediaYob}.

The rules of the world are as follows:

\begin{enumerate}
  \item The Explorer can move north (N), south (S), east (E), or west (W) in an $n \times n$ grid (the board).
  \item If the Explorer picks up the gold, the Explorer wins.
  \item If the Explorer enters a square with a Danger (here a Wumus or a Pit), the Explorer dies (loses).
  \item The Explorer possesses a single Arrow, which the Explorer may fire in a single direction. The Arrow travels until it hits the Wumpus or the edge of board. If it hits the Wumpus, the Wumpus dies.
  \item The Explorer can perceive a breeze from a Pit, a stench from the Wumpus, and a glitter from the gold. The breeze and stench perceptions are in the squares N, S, E, and W, adjacent to Dangers (not diagonally). The glitter from the gold is only in the square occupied by the gold, so the agent can only find it by being in the same square as the gold.
  \item The Explorer keeps a Journal, a knowledge base of information about the world. The Explorer may add to or retrieve information from the Journal to inform decisions.
  \item In some versions of the problem, the Gold can be in a pit, and the board is unsolvable. In my implementation, this is not possible, though it would make for an intriguing variation where the explorer can win by exploring every square possible and reporting that the world is not solvable.
\end{enumerate}

In my version of a solution to this problem, the Explorer follows a set of rules to determine where the Dangers lie, and uses a set of simple heuristics to avoid those dangers and maximize his chances of surviving to find the gold.

\section{Constructing the World}

The board is initialized with a particular size, which is the number of units for a single wall of the grid. The default is 10 (producing a $10 \times 10$ grid). A starting position can also be specified, or the Explorer will start in the upper left corner. A list of random coordinates, excluding the starting position, is generated, and these coordinates are used to place the Dangers in the world.

When a Danger is placed, if it has a perception indicator such as a breeze or stench, those perception indicators are placed in the surrounding squares.

The Explorer is then initialized and provided his starting position. He will then begin to explore the world in accordance with his rule set.

\section*{Explorer's Exploration Algorithm}

The Explorer, once being placed in the world and instructed to Explorer, will do the following:

\begin{enumerate}
	\item Visit() a square.
  	\begin{itemize}
   		\item If the square is an end-state (a Danger or the Gold), stop exploring and report success or failure.
		\item If the environment contains any Danger perceptions (such as a breeze or stench), mark the nearby squares for possible Dangers.
		\item Otherwise the current square and the adjacent squares to the N, S, E, and W are safe: Mark them as such.
	\end{itemize}
	\item Decide() where to move, or whether to fire the Arrow, by evaluating costs of moving to an adjacent square. This process is described below.
	\item Move() to the square with the lowest cost and loop.
\end{enumerate}

\section*{Explorer's Decide() Function}

The Explorer has some basic costs (or incentives) for moving:

\begin{itemize}
   	\item unvisited: -3
	\item safe: -2
	\item visit: 1
	\item per-visit: 50
\end{itemize}

The unvisited cost is the lowest possible cost; if the Explorer has no indication of danger in an unvisited square, that square will certainly be among the lowest cost moves available. If the Explorer has determined that a square is safe, they will also prefer that square even over other unvisited squares. The visit cost is the cost of a square that has been visited; it ensures a positive value for any visited square. The per visit cost must be large enough to discourage the Explorer from visiting the same squares repeatedly. It could be higher than the value I've chosen, but it must not be too high or the Explorer may accidentally choose certain danger over a square with only possible danger.

Each Danger has an associated set of costs. These are:

\begin{itemize}
   	\item horrible: 1000 (The cost for encountering the Danger.)
	\item cost: 500 (The cost associated with a possible Danger.)
	\item per: 50 (The cost for each indication the Explorer has of the Danger.)
\end{itemize}

The horrible cost must be high enough that the Explorer will never move to that square even if there is another square that might be dangerous nearby. The per cost is a multiplier based on the Explorer's Journal entries; if the Explorer has, for instance, encountered three breezes adjacent to a square, they will penalize moving to that square more than they would moving to a square that was adjacent to only a single breeze. The ``cost" entry is simply the basic cost for a possible danger, that is, a danger that the Explorer has not determined is absolutely certain.

The cost structure ensures that the Explorer will not wander forever. For instance, in the tables provided, the Explorer will enter an unvisited but potentially dangerous square after visiting a square with the perception indicator for that square 10 times. For example, the Explorer detects a danger and would choose between revisiting a square for the 10th time (with a cost of 500) or a possible Pit in an unvisited square (with a cost of $500 - 3 = 497$), the Explorer will choose the possibly dangerous square rather than wander forever.

The Explorer then evaluates the costs and ranks the costs for any particular square. The lowest costs are retained and the final decision process is as follows:

\begin{itemize}
   	\item If the Explorer has positively identified the Wumpus, shoot it and then Decide()
	\item If there is only a \textit{single} lowest-cost move, make that move.
	\item If the Explorer does not have a safe move and is next to the Wumpus, shoot in the lowest-cost direction the Wumpus could be. If there is a tie, choose randomly.
	\item Otherwise, choose a move randomly.
\end{itemize}

Shooting the Arrow can provide the Explorer with information even if the Wumpus does not die: The Explorer then marks the remaining possibilities as having a Wumpus. This most often occurs when the Wumpus is next to the Explorer's starting position; there are only two squares the Wumpus could be in, so the Explorer flips a coin and can positively identify its location whether or not the Explorer ``wins the coin flip." In exceptionally rare circumstances, the Explorer can enter a square next to the Wumpus and have three possible choices. For instance, they approach a Wumpus in a straight line without exploring other nearby squares.) In such a case, the Explorer may eventually be able to prove that a square does not contain the Wumpus. However, on smaller boards, if the Explorer cannot reach the gold without moving past one of the uncertain squares, they may eventually flip a coin and hope to get lucky moving past the Wumpus.

Some boards are simply impossible for the Explorer to win; for instance, if the Explorer starts surrounded by Pits. Note, also, that the Explorer can do no better than a coin flip when starting next to a Pit, because firing an Arrow doesn't give any new information.

Note that because the Explorer moves randomly in the absence of a clear decision, that the exploration is not completely systematic. If the randomization is taken away, however, the exploration will generally be a depth-first search, since the Explorer will always favor the first cardinal direction (in the provided implementation, that vector is South) whenever available before backtracking.

\section{Time Complexity Analysis}

We can make a cursory analysis of the Explorer's decision process to show that it is $O(n^2)$, where $n$ is the size of the board, for the Explorer to solve the environment.

We note that to decide on a move, the Explorer must examine the four surrounding squares, which is a constant multiple of 4. We use a hash table (dictionary in Python) for looking up information on a square, so lookup and adding to the knowledge base is constant time. The Explorer may occasionally be forced to revisit nearby squares until their ``visited" count for the square, multiplied by the per-visit cost, exceeds that of a possible danger and forces them to make a move that they are avoiding; but this is, again, a constant multiple, and that multiple can be adjusted by changing the per-visit value in the cost table above. (It is $\approx10$ with the values given.) Finally, the Explorer may be asked to examine at most 4 additional Journal entries when visiting a square, which is another constant multiple. A move action is simply adding a vector to a coordinate pair, which is constant time.

Since all the decision and move actions involve constant time, but the explorer must make them for each square visited, the time taken is $O(kn^2) = O(n^2)$ (although in practice, the constant $k$ can be fairly large). Redrawing the board after a move is also $O(n^2)$. The Explorer must also update the journal for every square once the Wumpus is killed (or found), which is $O(n^2)$.

\section{Program Implementation}

A UML diagram is provided in Fig. 1.

The program is run from the command line with 
\begin{lstlisting}
python3 WumpusWorld.py -s _ -sp _ _
\end{lstlisting}
where -s and -sp are optional, and the \_ are integers. For instance,
 \begin{lstlisting}
 python3 WumpusWorld.py -s 5
 \end{lstlisting}
 will run the program with a 5x5 board and the explorer starting in the default square, and
 \begin{lstlisting}
 python3 WumpusWorld.py -sp 0 5
 \end{lstlisting}
 will run the program with the default size board and the explorer starting in (0, 5).

\section{Discussion}

One can certainly ask ``how intelligent is the Agent?" for this program. As a rules-based implementation, the answer is ``not very." While the Explorer is quite successful in avoiding danger, and while the code permits some generalization should further Dangers be added, if the Danger cannot be treated like a Pit or a Wumpus, the Explorer is at a loss as to what to do without being explicitly programmed additional rules. In other words, the Agent doesn't learn and is just a dumb automaton, even if it can reliably solve a ``fair" random instance of its world.

As far as the code and design goes, the system is partially extensible, but new Dangers might require different algorithms. Since there are only two Dangers in this version, I chose to use if-then statements to deal with the differences between them. However, if the system were more complicated, then the explorer might have numerous algorithms for dealing with Dangers. Ideally then the program would use the Adapter design pattern. The Explorer's journal-filling methods when visiting a square would be moved to a class that is responsible for implementing a different algorithm based on the type of Danger perceived. (The perception, e.g. a Breeze, would likely become an object then, rather than just being part of the Danger object.)

How might this extension be imagined, if, say, the Bats from the 1973 game (which teleport the user to a random location) were added as a Danger? An Explorer might read a sign when first entering the cave: ``There are Pits, a Wumpus, and Bats in this cave." The explorer would then instantiate an object for marking squares filled with Bats, another for dealing with the Wumpus, and another for dealing with Pits. The decide() function does not need to change much; the explorer can just iterate through different types of Dangers and update the costs accordingly.

\begin{figure}[!htb] 
	\center{\includegraphics[width=\textwidth] 
	{WumpusWorldUML.png}} 
	\caption{\label{fig:my-label} UML Diagram for my Wumpus World implementation, drawn in Gliffy.}
\end{figure}

\begin{thebibliography}{}
\bibitem{RussellAndNorvig}Russell, S and Norvig, P (2002) \textit{Artificial Intelligence: A Modern Approach.} Second Edition. Prentice Hall.
\bibitem{wikipediaYob}Wikimedia Foundation. ``Hunt the Wumpus." Retrieved from \url{https://en.wikipedia.org/wiki/Hunt_the_Wumpus}.

\end{thebibliography}
\end{document}  