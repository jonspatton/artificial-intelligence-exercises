The program is run from the command line with 

"python WumpusWorld.py"

for the default setup

or

"python WumpusWorld.py -s _ -sp _ _" 

where -s and -sp are optional, and the _ are integers.

For instance, "python WumpusWorld.py -s 5" will run the program with a 5x5 board
and the explorer starting in the default square.

"python WumpusWorld.py -sp 0 5" will run the program with the default size board
and the explorer starting in (0, 5).

See WumpusAgentInPython.pdf for more information about the program.