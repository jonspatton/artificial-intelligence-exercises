from wumpus_utils import *

class Reward:
    ''' Base class for a good thing for the explorer.'''
    cost = None
    def __init__(self):
        self.cost = -1000

class Gold(Reward):
    ''' The end goal in the Wumpus World'''
    location = None

    ##################################
    # Representations and getters.
    ##################################
    def __init__(self, location = None):
        Reward.__init__(self)
        self.location = location
    
    def __repr__(self):
        return self.__str__()
    
    def __str__(self):
        return 'G'

    def getLocation(self):
        return self.location