# Fall 2012 6.034 Lab 2: Search
#
# Your answers for the true and false questions will be in the following form.  
# Your answers will look like one of the two below:
#ANSWER1 = True
#ANSWER1 = False

# 1: True or false - Hill Climbing search is guaranteed to find a solution
#    if there is a solution
ANSWER1 = False

# 2: True or false - Best-first search will give an optimal search result
#    (shortest path length).
#    (If you don't know what we mean by best-first search, refer to
#     http://courses.csail.mit.edu/6.034f/ai3/ch4.pdf (page 13 of the pdf).)
ANSWER2 = False

# 3: True or false - Best-first search and hill climbing make use of
#    heuristic values of nodes.
ANSWER3 = True

# 4: True or false - A* uses an extended-nodes set.
ANSWER4 = True

# 5: True or false - Breadth first search is guaranteed to return a path
#    with the shortest number of nodes.
ANSWER5 = True

# 6: True or false - The regular branch and bound uses heuristic values
#    to speed up the search for an optimal path.
ANSWER6 = False

# Import the Graph data structure from 'search.py'
# Refer to search.py for documentation
from search import Graph

## Optional Warm-up: BFS and DFS
# If you implement these, the offline tester will test them.
# If you don't, it won't.
# The online tester will not test them.

from graphs import *
from search import *

def bfs(graph, start, goal):
    return xfs(graph, start, goal, search_type = 'b')

## Once you have completed the breadth-first search,
## this part should be very simple to complete.
def dfs(graph, start, goal):
    return xfs(graph, start, goal, search_type = 'd')

# With the queue implementation, BFS and DFS differ on in where
# they insert the new paths (front versus back)
# 'Type' is 'd' = depth or 'b' = breadth, 'A*', 'beam', or 'branch_and_bound'
def xfs(graph, start, goal, *args, **kwargs):
    search_type = kwargs.get('search_type', None)
    k = kwargs.get('k', None)
    
    # Form a one-element queue consistning of a zero-length path
    # that contains only the root node.
    q = []
    q.insert(0, (start,))

    # Memoire visited nodes in A* and other searches that keep track of where
    # they've been.
    extendedSet = {}

    #Until the queue is empty ...
    while q:
        l = len(q)
        # Remove the first path from the queue ...
        path = q.pop(0)
        h = 0

        # If the goal node is found, announce success
        # (The lab asks that this is returned here rather than when
        # the correct path is found, which is important for algorithms
        # that return optimal paths rather than the first correct path.
        # However, in practice at least for this lab it never comes up.)
        if path[-1] == goal:
            return list(path)
        
        connected = graph.get_connected_nodes(path[-1])
        
        #For hill-climbing: sort the connected nodes by their distance from the goal
        if connected and search_type.lower() != 'd' and search_type.lower() != 'b':
            c_dict = {}
            for c in connected:
                if c not in path:
                    h = graph.get_heuristic(c, goal)
                    if search_type == 'A*':
                        c_dict[c] = h + path_length(graph, path)
                    else:
                        c_dict[c] = h
            connected = sorted(c_dict, key = c_dict.get)
            connected.reverse()
            if search_type.lower() == 'beam':
                connected = connected[len(q) - k:]

        # and create new paths by extending the first path 
        # to all neighbors of the terminal node
        for node in connected:
            if node not in path:
                newPath = path + (node,)
                plen = path_length(graph, newPath)                
                if search_type == 'A*':
                    h = graph.get_heuristic(node, goal)
                    if node not in extendedSet:
                        extendedSet[node] = (newPath, plen)
                        q = sortPath(q, newPath, graph, goal)
                    elif plen < extendedSet.get(node)[1]:
                        extendedSet[node] = (newPath, plen)
                        q = sortPath(q, newPath, graph, goal)
                        
                #BFS and derivatives: Add to end of q
                elif search_type.lower() == 'b' or search_type.lower() == 'beam':
                    q.append(newPath)
                #DFS and derivatives: Add to front of q
                else:
                    if search_type == 'branch_and_bound':
                        # Insert the new path only if it's the shortest way there.
                        if checkForShorterPaths(graph, q, node, plen):
                            q = sortPath(q, newPath, graph, goal)
                    else:
                        q.insert(0, newPath)
    return []

# For A* and B&B, we want to remove all but the shortest ways to get to a path.
# The best way to do this is to never insert them in the first place if there's
# a quicker way there.
def checkForShorterPaths(graph, q, node, newPathLength):
    for path in q:
        for n in path:
            if node == n:
                plen = path_length(path, n)
                # If we found a shorter existing path in the q, return false so we
                # know not to insert it                
                if plen < newPathLength:
                    return False
                # If our new path is shorter, then we want to get rid of the 
                # existing path.
                elif plen > newPathLength:
                    q.remove(path)
    #The remaining case is a tie, so we should keep them both and insert FILO
    return True

# There's no need to sort for A* if we always insert the new path in a sorted 
# position. This is O(log n) binary search to find the right spot for the new
# path, but inserting into a list is O(n), so we can't do any better.
def sortPath(q, newPath, graph, goal):
    if not q:
        q.insert(0, newPath)
        return q
    done = False
    h = graph.get_heuristic(newPath[-1], goal)
    plen = path_length(graph, newPath) + h
    end = len(q) - 1
    bot = 0
    while not done:
        mid = (end + bot)//2
        pi = q[mid]
        comp = path_length(graph, pi) + graph.get_heuristic(pi[-1], goal)
        if comp == plen or bot >= end:
            q.insert(mid + 1, newPath)
            done = True
        elif comp > plen:
            end = mid - 1
        else:
            bot = mid + 1
    return q

#print(dfs(GRAPH1, 'Common Area', 'Dungeon 5'))

#print(bfs(GRAPH1, 'Common Area', 'Dungeon 5'))

## Now we're going to add some heuristics into the search.  
## Remember that hill-climbing is a modified version of depth-first search.
## Search direction should be towards lower heuristic values to the goal.
def hill_climbing(graph, start, goal):
    return xfs(graph, start, goal, search_type = 'hill')

#print(hill_climbing(NEWGRAPH2, 'S', 'G'))
                
## Now we're going to implement beam search, a variation on BFS
## that caps the amount of memory used to store paths.  Remember,
## we maintain only k candidate paths of length n in our agenda at any time.
## The k top candidates are to be determined using the 
## graph get_heuristic function, with lower values being better values.
def beam_search(graph, start, goal, beam_width):
    return xfs(graph, start, goal, search_type = 'beam', k = beam_width)

## Now we're going to try optimal search.  The previous searches haven't
## used edge distances in the calculation.

## This function takes in a graph and a list of node names, and returns
## the sum of edge lengths along the path -- the total distance in the path.
def path_length(graph, node_names):
    nodeNum = len(node_names)
    length = 0
    for i in range(nodeNum - 1):
        edge = graph.get_edge(node_names[i], node_names[i + 1])
        length += edge.length
    return length

def branch_and_bound(graph, start, goal):
    return xfs(graph, start, goal, search_type = 'branch_and_bound')
def a_star(graph, start, goal):
    return xfs(graph, start, goal, search_type = 'A*')
#print(a_star(NEWGRAPH4, "S", "T"))

## It's useful to determine if a graph has a consistent and admissible
## heuristic.  You've seen graphs with heuristics that are
## admissible, but not consistent.  Have you seen any graphs that are
## consistent, but not admissible?

def is_admissible(graph, goal):
    # the heuristic value for every node in a graph must be <= the 
    # distance of the shortest path from the goal to that node

    #List of nodes:
    nodes = []
    for edge in graph.edges:
        n = edge.node1
        if n not in nodes:
            nodes += n
    
    for node in nodes:
        h = graph.get_heuristic(node, goal)
        path = a_star(graph, node, goal)
        plen = path_length(graph, path)
        if h > plen:
            return False
    return True

def is_consistent(graph, goal):
    # for each edge in the graph, the edge length must be >= 
    # the absolute value of the difference between the two 
    # heuristic values of its nodes
    for edge in graph.edges:
        n1 = edge.node1
        n2 = edge.node2
        diff = abs(graph.get_heuristic(n1, goal) - graph.get_heuristic(n2, goal))
        if edge.length < diff:
            return False
    return True

HOW_MANY_HOURS_THIS_PSET_TOOK = 9
WHAT_I_FOUND_INTERESTING = 'A*, consistent/admissible, and beam search'
WHAT_I_FOUND_BORING = 'True or false'
