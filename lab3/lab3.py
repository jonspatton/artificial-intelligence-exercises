# 6.034 Fall 2010 Lab 3: Games
# Name: <Your Name>
# Email: <Your Email>

from util import INFINITY

### 1. Multiple choice

# 1.1. Two computerized players are playing a game. Player MM does minimax
#      search to depth 6 to decide on a move. Player AB does alpha-beta
#      search to depth 6.
#      The game is played without a time limit. Which player will play better?
#
#      1. MM will play better than AB.
#      2. AB will play better than MM.
#      3. They will play with the same level of skill.
ANSWER1 = 3

# 1.2. Two computerized players are playing a game with a time limit. Player MM
# does minimax search with iterative deepening, and player AB does alpha-beta
# search with iterative deepening. Each one returns a result after it has used
# 1/3 of its remaining time. Which player will play better?
#
#   1. MM will play better than AB.
#   2. AB will play better than MM.
#   3. They will play with the same level of skill.
ANSWER2 = 2

### 2. Connect Four
from connectfour import *
from basicplayer import *
from util import *
import tree_searcher

## This section will contain occasional lines that you can uncomment to play
## the game interactively. Be sure to re-comment them when you're done with
## them.  Please don't turn in a problem set that sits there asking the
## grader-bot to play a game!
## 
## Uncomment this line to play a game as white:
#run_game(human_player, basic_player)

## Uncomment this line to play a game as black:
#run_game(basic_player, human_player)

## Or watch the computer play against itself:
#run_game(basic_player, basic_player)

## Change this evaluation function so that it tries to win as soon as possible,
## or lose as late as possible, when it decides that one side is certain to win.
## You don't have to change how it evaluates non-winning positions.

def focused_evaluate(board):
    """
    Given a board, return a numeric rating of how good
    that board is for the current player.
    A return value >= 1000 means that the current player has won;
    a return value <= -1000 means that the current player has lost
    """ 
    me = board.get_current_player_id()
    you = board.get_other_player_id()
    my_chain = chain_len = board.longest_chain(me)
    their_chain = chain_len = board.longest_chain(you)
    
    # If one of the chains is length 4, someone won, so the score has to be >=1000
    # for someone. But if we can win in 1 turn or 2 turns, we want to win in 1 turn.
    # When the moves are evaluated, later moves will have more tokens on the board.
    # So we can penalize the maximum score by subtracting the number of tokens in play.
    
    h = board.board_height
    w = board.board_width

    board_size = w * h
    win_base = 1000 + board_size
    tokens_in_play = board.num_tokens_on_board()
    
    if my_chain == 4:
        score = win_base - tokens_in_play
    elif their_chain == 4:
        score = tokens_in_play - win_base
    else:
        score = my_chain * 10 - tokens_in_play

        # Prefer having your pieces in the center of the board.
        for row in range(h):
            for col in range(w):
                if board.get_cell(row, col) == board.get_current_player_id():
                    score -= abs(3-col)
                elif board.get_cell(row, col) == board.get_other_player_id():
                    score += abs(3-col)
    return score


## Create a "player" function that uses the focused_evaluate function
quick_to_win_player = lambda board: minimax(board, depth=4,
                                            eval_fn=focused_evaluate)

## You can try out your new evaluation function by uncommenting this line:
#run_game(basic_player, quick_to_win_player)

## Write an alpha-beta-search procedure that acts like the minimax-search
## procedure, but uses alpha-beta pruning to avoid searching bad ideas
## that can't improve the result. The tester will check your pruning by
## counting the number of static evaluations you make.
##
## You can use minimax() in basicplayer.py as an example.

def alpha_beta_search(board, depth,
                      eval_fn,
                      # NOTE: You should use get_next_moves_fn when generating
                      # next board configurations, and is_terminal_fn when
                      # checking game termination.
                      # The default functions set here will work
                      # for connect_four.
                      get_next_moves_fn=get_all_next_moves,
		      is_terminal_fn=is_terminal):
    alpha = NEG_INFINITY
    beta = INFINITY
    
    best_move = None
    best_value = NEG_INFINITY
    moves = get_next_moves_fn(board)
    
    for move, new_board in moves:
        move_value = -alpha_beta_procedure(new_board, depth-1, eval_fn, alpha, beta, get_next_moves_fn, is_terminal_fn)
        #move_value = -alpha_beta_min_value(new_board, depth-1, eval_fn, alpha, beta, get_next_moves_fn, is_terminal_fn)

        if best_value == None or move_value > best_value:
            best_value = move_value
            best_move = move
    return best_move

# From http://aima.cs.berkeley.edu/python/games.html
# Plus the hint from the lab
def alpha_beta_procedure(board, depth, eval_fn, alpha, beta,
                get_next_moves_fn=get_all_next_moves, is_terminal_fn=is_terminal):

    #Base case: Usually when depth is 0
    if is_terminal_fn(depth, board):
        return eval_fn(board)
    
    # Recursive case: Per the hint, swap alpha and beta and negate them, to treat
    # every level as a maximizing case.
    next_moves = get_next_moves_fn(board)
    v = NEG_INFINITY
    for move, new_board in next_moves:
        v = max(v, alpha_beta_procedure(new_board, depth - 1, eval_fn,
                            -beta, -alpha, get_next_moves_fn, is_terminal_fn))
        if v >= beta:
            return v
        alpha = max(alpha, v)
    return v

# From http://aima.cs.berkeley.edu/python/games.html
# These don't work quite as well for some reason ...
def alpha_beta_max_value(board, depth, eval_fn, alpha, beta,
                get_next_moves_fn=get_all_next_moves, is_terminal_fn=is_terminal):
    #Base case: Usually when depth is 0
    if is_terminal_fn(depth, board):
        return eval_fn(board)
    
    # Recursive case
    next_moves = get_next_moves_fn(board)
    v = NEG_INFINITY
    for move, new_board in next_moves:
        v = max(v, alpha_beta_min_value(new_board, depth - 1, eval_fn,
                            alpha, beta, get_next_moves_fn, is_terminal_fn))
        if v >= beta:
            return v
        alpha = max(alpha, v)
    return v


def alpha_beta_min_value(board, depth, eval_fn, alpha, beta,
                get_next_moves_fn=get_all_next_moves, is_terminal_fn=is_terminal):
    
    #Base case: Usually when depth is 0
    if is_terminal_fn(depth, board):
        return eval_fn(board)
    
    # Recursive case
    next_moves = get_next_moves_fn(board)
    v = INFINITY
    for move, new_board in next_moves:
        v = min(v, alpha_beta_max_value(new_board, depth - 1, eval_fn,
                            alpha, beta, get_next_moves_fn, is_terminal_fn))
        if v <= alpha:
            return v
        beta = min(beta, v)
    return v

## Now you should be able to search twice as deep in the same amount of time.
## (Of course, this alpha-beta-player won't work until you've defined
## alpha-beta-search.)
alphabeta_player = lambda board: alpha_beta_search(board,
                                                   depth=8,
                                                   eval_fn=focused_evaluate)

## This player uses progressive deepening, so it can kick your ass while
## making efficient use of time:
ab_iterative_player = lambda board: \
    run_search_function(board,
                        search_fn=alpha_beta_search,
                        eval_fn=focused_evaluate, timeout=5)
#run_game(human_player, alphabeta_player)

## Finally, come up with a better evaluation function than focused-evaluate.
## By providing a different function, you should be able to beat
## simple-evaluate (or focused-evaluate) while searching to the
## same depth.

# Connect Four is a solved game. The heuristic involves calculating
# the parity of threats and working toward forcing a move.
# See: http://web.mit.edu/sp.268/www/2010/connectFourSlides.pdf
# and http://www.informatik.uni-trier.de/~fernau/DSL0607/Masterthesis-Viergewinnt.pdf
# originally solved by James Allen http://fabpedigree.com/james/connect4.htm
def better_evaluate(board):
    f = focused_evaluate(board)
    return f

# This heuristic from John Wang @ MIT.
# https://github.com/wangjohn/mit-courses/blob/master/6.034-labs/lab3/lab3.py
# However, this doesn't actually beat the computer often enough.
# It appears to be searching for the best chain that can be formed from
# any particular move in each square and increasing or penalizing the score
# accordingly.

def better_evaluate1(board):
    score = 0
    if board.longest_chain(board.get_current_player_id()) == 4:
        score = 2000 - board.num_tokens_on_board()
    elif board.longest_chain(board.get_other_player_id()) == 4:
        score = -2000 + board.num_tokens_on_board()
    else:
        current_player = board.get_current_player_id()
        other_player = board.get_other_player_id()
        for row in xrange(3):
            for col in xrange(4):
                score += max([get_chain_len(i, board, row, col, current_player) for i in xrange(3)])**2
                score -= max([get_chain_len(i, board, row, col, other_player) for i in xrange(3)])**2
    return score

def get_chain_len(chain_type, board, row, col, player):
    count = 0
    for i in xrange(3):
        if chain_type == 0 and board.get_cell(row, col+i) == player:
            count += 1
        elif chain_type == 1 and board.get_cell(row+i, col) == player:
            count += 1
        elif chain_type == 2 and board.get_cell(row+i, col+i) == player:
            count += 1
    return count

# Comment this line after you've fully implemented better_evaluate
#better_evaluate = memoize(basic_evaluate)

# Uncomment this line to make your better_evaluate run faster.
better_evaluate = memoize(better_evaluate)

# For debugging: Change this if-guard to True, to unit-test
# your better_evaluate function.
if False:
    board_tuples = (( 0,0,0,0,0,0,0 ),
                    ( 0,0,0,0,0,0,0 ),
                    ( 0,0,0,0,0,0,0 ),
                    ( 0,2,2,1,1,2,0 ),
                    ( 0,2,1,2,1,2,0 ),
                    ( 2,1,2,1,1,1,0 ),
                    )
    test_board_1 = ConnectFourBoard(board_array = board_tuples,
                                    current_player = 1)
    test_board_2 = ConnectFourBoard(board_array = board_tuples,
                                    current_player = 2)
    # better evaluate from player 1
    print "%s => %s" %(test_board_1, better_evaluate(test_board_1))
    # better evaluate from player 2
    print "%s => %s" %(test_board_2, better_evaluate(test_board_2))

## A player that uses alpha-beta and better_evaluate:
your_player = lambda board: run_search_function(board,
                                                search_fn=alpha_beta_search,
                                                eval_fn=better_evaluate,
                                                timeout=5)

#your_player = lambda board: alpha_beta_search(board, depth=4,
#                                              eval_fn=better_evaluate)

## Uncomment to watch your player play a game:
#run_game(your_player, your_player)

## Uncomment this (or run it in the command window) to see how you do
## on the tournament that will be graded.
#run_game(ab_iterative_player, basic_player)

## These three functions are used by the tester; please don't modify them!
def run_test_game(player1, player2, board):
    assert isinstance(globals()[board], ConnectFourBoard), "Error: can't run a game using a non-Board object!"
    return run_game(globals()[player1], globals()[player2], globals()[board])
    
def run_test_search(search, board, depth, eval_fn):
    assert isinstance(globals()[board], ConnectFourBoard), "Error: can't run a game using a non-Board object!"
    return globals()[search](globals()[board], depth=depth,
                             eval_fn=globals()[eval_fn])

## This function runs your alpha-beta implementation using a tree as the search
## rather than a live connect four game.   This will be easier to debug.
def run_test_tree_search(search, board, depth):
    return globals()[search](globals()[board], depth=depth,
                             eval_fn=tree_searcher.tree_eval,
                             get_next_moves_fn=tree_searcher.tree_get_next_move,
                             is_terminal_fn=tree_searcher.is_leaf)
    
## Do you want us to use your code in a tournament against other students? See
## the description in the problem set. The tournament is completely optional
## and has no effect on your grade.
COMPETE = False

## The standard survey questions.
HOW_MANY_HOURS_THIS_PSET_TOOK = 16
WHAT_I_FOUND_INTERESTING = 'alpha-beta'
WHAT_I_FOUND_BORING = "Nothing"
NAME = "Jon"
EMAIL = ""

