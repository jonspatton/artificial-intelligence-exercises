from classify import *
import math

##
## CSP portion of lab 4.
##
from csp import BinaryConstraint, CSP, CSPState, Variable,\
    basic_constraint_checker, solve_csp_problem

# Implement basic forward checking on the CSPState see csp.py
def forward_checking(state, verbose=False):
    # Before running Forward checking we must ensure
    # that constraints are okay for this state.
    basic = basic_constraint_checker(state, verbose)
    if not basic:
        return False

    # Add your forward checking logic here.
    # 1. Let X be the current variable being assigned.
    X = state.get_current_variable()

    # 2. Let x be the value being assigned to X.
    x = None

    if X is not None:
        x = X.get_assigned_value()
    else:
        return True

    # 3. Find all the binary constraints that are associated with X.
    constraints = state.get_constraints_by_name(X.get_name())

    # 4. For each constaint: 
    for constraint in constraints:
    #   1. Let Y be the variable connected to X 
    #   by that binary constraint.

        #X is always the "i" in the constraint, so we want Y to be the "j" component
        Y = state.get_variable_by_name(constraint.get_variable_j_name())
        Y_domain = Y.get_domain()

    #   2. for each variable y in Y's domain:
        for y in Y_domain:
    #       1. if constraint checking fails for X = x and Y = y
    #           remove y from Y's domain
            if not constraint.check(state, x, y):
                Y.reduce_domain(y)
    #       2. If the domain of Y is reduced to the empty set,
    #           the entire check fails; return False.
            if not Y.get_domain():
                return False
    # 5. If all constraints passed declare success, return True.
    return True

# Now Implement forward checking + (constraint) propagation through
# singleton domains.
def forward_checking_prop_singleton(state, verbose=False):
    # 1. Run forward checking first.
    fc_checker = forward_checking(state, verbose)
    if not fc_checker:
        return False

    # 2. Find variables with domain size 1.
    # 3. Create a queue of singleton variables.
    singletonVars = []
    for var in state.get_all_variables():
        if var.domain_size() == 1:
            singletonVars.append(var)

    visited = {}
    while singletonVars:
        X = singletonVars.pop()
    #   1. Pop off the first singleton variable X (add X to list of visited singletons)
        x = X.get_domain()[0]
        visited.update({X : x})

    #   2. Find all the binary constraints that singleton X is associated with.
        constraints = state.get_constraints_by_name(X.get_name())
    
    #   3. For each constraint therein:
        for constraint in constraints:
    #       1. Let Y be the variable connected to X by that binary constraint:
            Y = state.get_variable_by_name(constraint.get_variable_j_name())
            Y_domain = Y.get_domain()
    #           1. For each value of y in Y's domain:
            for y in Y_domain:
    #           1. If constraint check fails for X = (X's singleton value) 
    #           and Y = y: Remove y from Y's domain
                if not constraint.check(state, x, y):
                    Y.reduce_domain(y)
    #           2. If the domain of Y is reduced to the empty set,
    #              the entire check fails; return False.
                if not Y.get_domain():
                    return False
    #   4. Check to see if domain reduction produced any new and unvisited
    #      singletons; if so, add them to the queue.
                # (We can just add them during the for-each when they're created.)
                elif Y.domain_size() == 1 \
                    and Y not in visited:
                    singletonVars.insert(0, Y)
    return True

## The code here are for the tester
## Do not change.
from moose_csp import moose_csp_problem
from map_coloring_csp import map_coloring_csp_problem

def csp_solver_tree(problem, checker):
    problem_func = globals()[problem]
    checker_func = globals()[checker]
    answer, search_tree = problem_func().solve(checker_func)
    return search_tree.tree_to_string(search_tree)

##
## CODE for the learning portion of lab 4.
##

### Data sets for the lab
## You will be classifying data from these sets.
senate_people = read_congress_data('S110.ord')
senate_votes = read_vote_data('S110desc.csv')

house_people = read_congress_data('H110.ord')
house_votes = read_vote_data('H110desc.csv')

last_senate_people = read_congress_data('S109.ord')
last_senate_votes = read_vote_data('S109desc.csv')


### Part 1: Nearest Neighbors
## An example of evaluating a nearest-neighbors classifier.
senate_group1, senate_group2 = crosscheck_groups(senate_people)
#evaluate(nearest_neighbors(hamming_distance, 1), senate_group1, senate_group2, verbose=1)

## Write the euclidean_distance function.
## This function should take two lists of integers and
## find the Euclidean distance between them.
## See 'hamming_distance()' in classify.py for an example that
## computes Hamming distances.

def euclidean_distance(list1, list2):
    assert isinstance(list1, list)
    assert isinstance(list2, list)
    if len(list1) == len(list2):
        v = [(l1 - l2)**2 for l1, l2 in zip(list1, list2)]
        return sum(v)**0.5
    else:
        raise Exception("Vector lists must be the same length to \
                         compute Euclidean distance.")

#Once you have implemented euclidean_distance, you can check the results:
# k = 5 is only 2 errors; k = 3 or 4 is three errors.
# Interestingly, the errors aren't for the same Senators.
#evaluate(nearest_neighbors(euclidean_distance, 5), senate_group1, senate_group2, 1)

## By changing the parameters you used, you can get a classifier factory that
## deals better with independents. Make a classifier that makes at most 3
## errors on the Senate.

#my_classifier = nearest_neighbors(euclidean_distance, 5)
#evaluate(my_classifier, senate_group1, senate_group2, verbose=1)

### Part 2: ID Trees
#print CongressIDTree(senate_people, senate_votes, homogeneous_disorder)

## Now write an information_disorder function to replace homogeneous_disorder,
## which should lead to simpler trees.

def information_disorder(*branches):
    # Formula from Dr. Winston's book:
    # sum_b (n_b/s_t) * sum_c [(-n_bc / n_b) log_2 (n_bc/n_b)
    # Where b is a branch, c is a class, and t is the total
    # (n is the numbers of each)
    
    # The lab version only uses yes or no as classes, but *branches
    # generalizes to any number of input arguments.
    
    n_t = sum([len(branch) for branch in branches])
    
    #Get the sum terms for the classes for each branch.
    branch_sum_terms = [] #Sum_b stuff
    branch_class_sums = [] #Sum_c stuff
    final = 0
    for branch in branches:
        n_b = len(branch)
        branch_sum_terms.append(float(n_b)/n_t)
        classes = {}
        n_b = len(branch)
        for vote in branch:
            if vote not in classes:
                classes[vote] = 1
            else:
                classes[vote] += 1
        sum_c = 0
        for key in classes:
            sum_c += (-float(classes[key])/n_b) * math.log(float(classes[key])/n_b, 2)
        branch_class_sums.append(sum_c)
        final += sum_c * float(n_b)/n_t

    return final

    #return homogeneous_disorder(yes, no)

#Test cases from the lab doc.
# Expected: 0
#print information_disorder(["Democrat", "Democrat", "Democrat"],["Republican", "Republican"])
# Expected: 1.0
#print information_disorder(["Democrat", "Republican"], ["Republican", "Democrat"])


print CongressIDTree(senate_people, senate_votes, information_disorder)
#evaluate(idtree_maker(senate_votes, homogeneous_disorder), senate_group1, senate_group2)

## Now try it on the House of Representatives. However, do it over a data set
## that only includes the most recent n votes, to show that it is possible to
## classify politicians without ludicrous amounts of information.

def limited_house_classifier(house_people, house_votes, n, verbose = False):
    house_limited, house_limited_votes = limit_votes(house_people,
    house_votes, n)
    house_limited_group1, house_limited_group2 = crosscheck_groups(house_limited)

    if verbose:
        print "ID tree for first group:"
        print CongressIDTree(house_limited_group1, house_limited_votes,
                             information_disorder)
        print
        print "ID tree for second group:"
        print CongressIDTree(house_limited_group2, house_limited_votes,
                             information_disorder)
        print
        
    return evaluate(idtree_maker(house_limited_votes, information_disorder),
                    house_limited_group1, house_limited_group2)

                                   
## Find a value of n that classifies at least 430 representatives correctly.
## Hint: It's not 10.

# The classifiers aren't monotonic. There is no point in doing this by
# hand, so we'll just run some loops to find the smallest n that
# beat the threshold.

N_1 = 1
rep_classified = 0
while rep_classified < 430 and N_1 < 539:
    rep_classified = limited_house_classifier(house_people, house_votes, N_1)
    N_1 += 1

print N_1, rep_classified

## Find a value of n that classifies at least 90 senators correctly.
N_2 = 1
senator_classified = 0
while senator_classified < 90 and N_1 < 100:
    senator_classified = limited_house_classifier(senate_people, senate_votes, N_2)
    N_2 += 1

print N_2, senator_classified

## Now, find a value of n that classifies at least 95 of last year's senators correctly.
N_3 = 10
old_senator_classified = 0
while old_senator_classified < 95 and N_3 < 100:
    old_senator_classified = limited_house_classifier(last_senate_people, last_senate_votes, N_3)
    N_3 += 1

print N_3, old_senator_classified

## The standard survey questions.
HOW_MANY_HOURS_THIS_PSET_TOOK = "8"
WHAT_I_FOUND_INTERESTING = "The disorder funcion in particular, and figuring out how to make it generalizable to more than 2 classes"
WHAT_I_FOUND_BORING = "Nothing."


## This function is used by the tester, please don't modify it!
def eval_test(eval_fn, group1, group2, verbose = 0):
    """ Find eval_fn in globals(), then execute evaluate() on it """
    # Only allow known-safe eval_fn's
    if eval_fn in [ 'my_classifier' ]:
        return evaluate(globals()[eval_fn], group1, group2, verbose)
    else:
        raise Exception, "Error: Tester tried to use an invalid evaluation function: '%s'" % eval_fn

    
